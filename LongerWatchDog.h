#ifndef LongerWatchDog_h
#define LongerWatchDog_h

class LongerWatchDog
{
  public:
    static void config_mode();
    static void enable(int);
    static void disable();
    static void reset();
    static void sleep_ms(long);
    static void put_to_sleep();
    static void longest_timout_below(volatile long, volatile int&, volatile int&);
    static void interrupt_after(volatile int);
    static void device_reset_after_wdto(volatile int);
    static void device_reset_after_ms(volatile long);
    static void device_reset();
    static int prescaler_register(int);
  private:
};

#endif

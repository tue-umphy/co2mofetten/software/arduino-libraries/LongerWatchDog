# LongerWatchDog Arduino library

This library provides convenience routines wrapping around the low-level
watchdog routines.

## Capabilities

- resetting the Arduino after an **arbitrary** amount of miliseconds, not just
  the predefined discrete timeouts up to a maximum of 8 seconds but also
  beyond.
- putting the Arduino into sleep mode for an **arbitrary** amount of
  milliseconds (but still waking up temporarily at regular intervals).

## Debugging

If you define `LongerWatchDog_DEBUG` either in your build environment (e.g. by
defining `build_flags=-DLongerWatchDog_DEBUG` in your `platformio.ini` if you
are using [PlatformIO](https://platformio.org)) or directly at the top of
`LongerWatchDog.h` (`#define LongerWatchDog_DEBUG`), the library will
`Serial.print` out important steps it performs. In the `ISR`, only single
characters are printed for debugging purposes as [one should not use
`Serial.print` during
interrupts](https://forum.arduino.cc/index.php?topic=86485.0). You can still
force the usage of `Serial.print` in the ISR for a more clear description of
whats happening by defining `LongerWatchDog_ISR_SERIAL`.
